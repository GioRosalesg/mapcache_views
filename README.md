# Repositorio de prueba archivo de configuración

El presente repositorio es una prueba de configuración del archivo "mapcache.xlm" para Mapserver >= v6.0.0.

Este archivo funciona como guía de generación de **tiles estáticas** para distintas resoluciones (niveles de zoom) con el propósito de incluir en un visualizador de mapas utilizando el estándar **Web Map Service** (WMS de la OGC) mismas que ofrecen librerías como **Leaflet, Openlayers, ArcGIS Online**, o plataformas GIS de escritorios como **QGIS, GvSIG y ArcGIS**; haciendo ágil la visualización de los diferentes fragmentos que conforman el espacio geográfico deseado (configurado).

![tile example](https://gitlab.com/GioRosalesg/mapcache_views/-/raw/master/demo/WGS84/00/000/000/000/000/000/000.png)
